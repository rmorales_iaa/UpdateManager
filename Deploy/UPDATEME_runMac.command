#!/bin/bash
#change to the run directory
dir=$0
dir=${dir%/*}/
cd "$dir"

cd GSE
java -Djava.library.path=. -classpath GSE.jar:../jar/JChart/jcommon-1.0.16.jar:../jar/JChart/jfreechart-1.0.13.jar:../jar/MySQL/mysql-connector-java-5.1.7-bin.jar GSE
