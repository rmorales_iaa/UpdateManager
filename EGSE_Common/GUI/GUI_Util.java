//====================================================================
//  Description:Common methods for visual GUI 
//  File:GUI_Util.java
//  Package:EGSE_Common.GUI
//  Author:Rafael Morales Mu�oz
//  Date:05 March 2008
//  Version:0.1
//  History:none
//=====================================================================

//=====================================================================
//Package Name
//=====================================================================

package EGSE_Common.GUI;

//===================================================================== 
//User imports


//===================================================================== 
//System imports

import java.awt.Color;
import java.awt.Font;

import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

//===================================================================== 
//Class implementation

public class GUI_Util{
		
	//===========================================
	// fonts
	public static final Font TEXT_FONT = new Font( "Dialog", Font.PLAIN, 14 ); 

	// Colors	
	public static final Color COLOR_DEFAULT_FOREGROUND = new Color(51,51,51);
	public static final Color COLOR_DEFAULT_BACKGROUND = new Color(238,238,238);
    
	// Styles for textPanes
    public static Style STYLE_DEFAULT;
    private static Style STYLE_ERROR;
    private static Style STYLE_WARNING;
    private static Style STYLE_UNKNOWN;
    private static Style STYLE_INFO;
    private static Style STYLE_MONOSPACED_DEFAULT; 
    public static Style STYLE_MONOSPACED_ERROR;
    public static Style STYLE_MONOSPACED_WARNING;
    private static Style STYLE_MONOSPACED_UNKNOWN;
    private static Style STYLE_MONOSPACED_INFO;
    
	//===========================================	
	
	public static void initDocument(StyledDocument doc){    
    
        Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        STYLE_DEFAULT=doc.addStyle("DEFAULT",def);     
        StyleConstants.setFontFamily(STYLE_DEFAULT,"Courier New");
        StyleConstants.setAlignment(STYLE_DEFAULT,StyleConstants.ALIGN_RIGHT);        
        StyleConstants.setFontSize(STYLE_DEFAULT,14);
        StyleConstants.setSpaceAbove(STYLE_DEFAULT,2);
        StyleConstants.setSpaceBelow(STYLE_DEFAULT,2);   
        StyleConstants.setForeground(STYLE_DEFAULT,Color.BLACK);
                
        STYLE_ERROR=doc.addStyle("ERROR",def);    	
        StyleConstants.setFontFamily(STYLE_ERROR,"Courier New");
        StyleConstants.setAlignment(STYLE_ERROR,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_ERROR,14);
        StyleConstants.setSpaceAbove(STYLE_ERROR,2);
        StyleConstants.setSpaceBelow(STYLE_ERROR,2);   
        StyleConstants.setForeground(STYLE_ERROR,Color.RED);

        STYLE_WARNING=doc.addStyle("WARNING",def);    
        StyleConstants.setFontFamily(STYLE_WARNING,"Courier New");
        StyleConstants.setAlignment(STYLE_WARNING,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_WARNING,14);
        StyleConstants.setSpaceAbove(STYLE_WARNING,2);
        StyleConstants.setSpaceBelow(STYLE_WARNING,2);   
        StyleConstants.setForeground(STYLE_WARNING,Color.blue);

        STYLE_UNKNOWN=doc.addStyle("UNKNOWN",def);    	
        StyleConstants.setFontFamily(STYLE_UNKNOWN,"Courier New");
        StyleConstants.setAlignment(STYLE_UNKNOWN,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_UNKNOWN,14);
        StyleConstants.setSpaceAbove(STYLE_UNKNOWN,2);
        StyleConstants.setSpaceBelow(STYLE_UNKNOWN,2);   
        StyleConstants.setForeground(STYLE_UNKNOWN,Color.GRAY);        

        STYLE_INFO=doc.addStyle("INFO",def);    	 
        StyleConstants.setFontFamily(STYLE_INFO,"Courier New");
        StyleConstants.setAlignment(STYLE_INFO,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_INFO,14);
        StyleConstants.setSpaceAbove(STYLE_INFO,2);
        StyleConstants.setSpaceBelow(STYLE_INFO,2);   
        StyleConstants.setForeground(STYLE_INFO,Color.GREEN);        
        
        STYLE_MONOSPACED_DEFAULT=doc.addStyle("MONOSPACED_DEFAULT",def);        
        StyleConstants.setFontFamily(STYLE_MONOSPACED_DEFAULT,"Courier New");
        StyleConstants.setAlignment(STYLE_MONOSPACED_DEFAULT,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_MONOSPACED_DEFAULT,14);
        StyleConstants.setSpaceAbove(STYLE_MONOSPACED_DEFAULT,2);
        StyleConstants.setSpaceBelow(STYLE_MONOSPACED_DEFAULT,2);   
        StyleConstants.setForeground(STYLE_MONOSPACED_DEFAULT,Color.BLACK);
                
        STYLE_MONOSPACED_ERROR=doc.addStyle("MONOSPACED_ERROR",def);    	
        StyleConstants.setFontFamily(STYLE_MONOSPACED_ERROR,"Courier New");
        StyleConstants.setAlignment(STYLE_MONOSPACED_ERROR,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_MONOSPACED_ERROR,14);
        StyleConstants.setSpaceAbove(STYLE_MONOSPACED_ERROR,2);
        StyleConstants.setSpaceBelow(STYLE_MONOSPACED_ERROR,2);   
        StyleConstants.setForeground(STYLE_MONOSPACED_ERROR,Color.RED);        

        STYLE_MONOSPACED_WARNING=doc.addStyle("MONOSPACED_WARNING",def);   
        StyleConstants.setFontFamily(STYLE_MONOSPACED_WARNING,"Courier New");
        StyleConstants.setFontFamily(STYLE_MONOSPACED_WARNING,"Courier New");
        StyleConstants.setAlignment(STYLE_MONOSPACED_WARNING,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_MONOSPACED_WARNING,14);
        StyleConstants.setSpaceAbove(STYLE_MONOSPACED_WARNING,2);
        StyleConstants.setSpaceBelow(STYLE_MONOSPACED_WARNING,2);   
        StyleConstants.setForeground(STYLE_MONOSPACED_WARNING,Color.ORANGE);        

        STYLE_MONOSPACED_UNKNOWN=doc.addStyle("MONOSPACED_UNKNOWN",def);    	
        StyleConstants.setFontFamily(STYLE_MONOSPACED_UNKNOWN,"Courier New");
        StyleConstants.setAlignment(STYLE_MONOSPACED_UNKNOWN,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_MONOSPACED_UNKNOWN,14);
        StyleConstants.setSpaceAbove(STYLE_MONOSPACED_UNKNOWN,2);
        StyleConstants.setSpaceBelow(STYLE_MONOSPACED_UNKNOWN,2);   
        StyleConstants.setForeground(STYLE_MONOSPACED_UNKNOWN,Color.GRAY);        

        STYLE_MONOSPACED_INFO=doc.addStyle("MONOSPACED_INFO",def);    	
        StyleConstants.setFontFamily(STYLE_MONOSPACED_INFO,"Courier New");
        StyleConstants.setAlignment(STYLE_MONOSPACED_INFO,StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(STYLE_MONOSPACED_INFO,14);
        StyleConstants.setSpaceAbove(STYLE_MONOSPACED_INFO,2);
        StyleConstants.setSpaceBelow(STYLE_MONOSPACED_INFO,2);   
        StyleConstants.setForeground(STYLE_MONOSPACED_INFO,Color.GREEN);            
    }

    //===================================================================== 
}

//End of Class

//=====================================================================
//END OF FILE "GUI_Util.java"
