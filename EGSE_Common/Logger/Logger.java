//===================================================================== 
//  Description:Logger implementation.Writes a log entry into log file
//  Author:Rafael Morales Mu�oz
//  Date:15 April 2007
//  Version:0.1
//  History:none
//=====================================================================

//=====================================================================
//Package Name
//=====================================================================

package EGSE_Common.Logger;

//=====================================================================
//Imports
//=====================================================================

//User imports
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.ArrayBlockingQueue;

import EGSE_Common.Util.CommonConstant;
import EGSE_Common.Util.Util;


//=====================================================================
//Class Declaration
//=====================================================================

public class Logger extends Thread implements CommonConstant {

//=====================================================================                                                                                        
 //Private constants

	public static final String DEBUG_DIVIDER=            "[DBG]";
	public static final String DECO_DATABASE_DIVIDER=    "[DDB]";
	public static final String DECO_DIVIDER=             "[DEC]";
	public static final String DEFAULT_DIVIDER=          "[GEN]";
    public static final String DOWNLOAD_DIVIDER=         "[DOW]";
    public static final String ERROR_DIVIDER=            "[ERR]";
    public static final String INITIALIZATION_DIVIDER=   "[INI]";
    private static final String LOG_DIVIDER=              "[LOG]";
    public static final String PACKET_DIVIDER=           "[PAC]";
    public static final String PROGRAM_SYNTAX_DIVIDER=   "[PRG]";
    public static final String SERIAL_PORT_DIVIDER=      "[SER]";
    public static final String SESSION_DIVIDER=          "[SES]";
    public static final String UDP_DIVIDER=              "[UDP]";
    public static final String VISUALIZATION_DIVIDER=    "[VIS]";
    protected static final String WARNING_DIVIDER=      "[WAR]";    
    public static final String XML_DIVIDER=              "[XML]";
    public static final String XPATH_DIVIDER=            "[XPA]";
    public static final String TCP_DIVIDER=              "[TCP]";
    public static final String CODE_GENERATOR_DIVIDER=   "[GEN]";
    
    
    private static final int MAX_QUEUE_SIZE=             100;

//=====================================================================                                                                                        
//Private vars

    //Directories
    private static String mainDirectory;
    private static String currentDirectory;
    
    //Filename
    private static String currentFilename;
    
    //Log file
    private static BufferedWriter logFile;
    private static String logFileName;
        
    //Thread
     private static boolean stopRunning;
     
    //Message queue
    private static ArrayBlockingQueue <String> messageQueue;
    
    //sleep
    private static final int THREAD_TIMEOUT=                    100;
            
    //=====================================================================                
    
    public Logger(){
    	
    	super( Logger.class.getSimpleName() );
    }
    
    //=====================================================================
                        
    protected static void initializeAndStart(String path) {
                   
        //Storage           
        messageQueue=new ArrayBlockingQueue <String> (MAX_QUEUE_SIZE, true);

        //Create the name of the log file
        mainDirectory=path;
        Calendar rightNow = Calendar.getInstance();      
      
        String dirPath="",dirName="",month,day;
        int year;
      
        //Year
        year=rightNow.get(Calendar.YEAR);  
        dirName+=year;
        dirPath+=year+pathDivider;
      
        //Month
        month=MONTH[rightNow.get(Calendar.MONTH)];  
        dirName+=DASH+month;
        dirPath+=year+DASH+month+pathDivider;

        //Day
        day=Util.formatNumber(rightNow.get(Calendar.DAY_OF_MONTH),2);  
        dirName+=DASH+day+DASH+DAY[rightNow.get(Calendar.DAY_OF_WEEK)-1]; 
        dirPath+=year+DASH+month+DASH+day+pathDivider;
      
        currentDirectory=mainDirectory+pathDivider+dirPath;                                  
        boolean success = (new File(currentDirectory)).mkdirs();
      
        if (success) add("created directory:'"+ currentDirectory +"'",
                         LOG_DIVIDER);
        else add("File used for logging:'"+ currentDirectory +"'",LOG_DIVIDER);

        currentFilename=""+Util.formatNumber(rightNow.get(
                                                   Calendar.HOUR_OF_DAY),2)+"h";  
        currentFilename+=DASH+Util.formatNumber(
                                           rightNow.get(Calendar.MINUTE),2)+"m";  
        currentFilename+=DASH+Util.formatNumber(
                                           rightNow.get(Calendar.SECOND),2)+"s";  
        currentFilename+=DASH+Util.formatNumber(rightNow.get(
                                                  Calendar.MILLISECOND),3)+"ms";  
      
        logFileName=currentDirectory+pathDivider+dirName+DASH+
                  currentFilename+DOT+LOG_EXTENSION;
        openLog(logFileName);       
            
        //First message        
        add("Log file '"+path+"' created",LOG_DIVIDER);
        
        stopRunning=false;
        
        // Start the thread
        (new Logger()).start();
    }

    //=====================================================================   

    public void run(){
    
        while (!stopRunning){
        
        	String message=messageQueue.poll();
        	
        	if ( message != null ) Util.writeString(logFile,message+NEW_LINE);
        	else{        	
        		try {
                    sleep((long)THREAD_TIMEOUT);
                }
                catch(InterruptedException ex){
                    add("Logger thread has been interrupted while "+
                                "it was sleeping",Logger.PACKET_DIVIDER);
                    add(ex.getMessage(),Logger.ERROR_DIVIDER);
                }
            }
        }
    }
                            
    //=====================================================================   

    private static int openLog(String s){
    
        FileWriter f=null;
        try {           
            f=new FileWriter(s);
        }
        catch (IOException  e) {      
            add("Error:"+e.getMessage()+"Can not open the ouput file:'" +s+"'",
                LOG_DIVIDER);
            return -1;
        }
        logFile=new BufferedWriter(f);    
        return 1;
    }
  
   //=====================================================================   
    
    private static String createEntry(String s,String separator) {        
 
        Calendar rightNow = Calendar.getInstance();     
        StringBuffer logEntry = new StringBuffer( Util.formatNumber(rightNow.get(Calendar.YEAR),4) );
        logEntry.append(DOT);
        logEntry.append(Util.formatNumber(rightNow.get(Calendar.MONTH)+1,2));
        logEntry.append(DOT);
        logEntry.append(Util.formatNumber(rightNow.get(Calendar.DAY_OF_MONTH),2));
        logEntry.append(" ");
        logEntry.append(Util.formatNumber(rightNow.get(Calendar.HOUR_OF_DAY),2));
        logEntry.append("h ");
        logEntry.append(Util.formatNumber(rightNow.get(Calendar.MINUTE),2));
        logEntry.append("m ");
        logEntry.append(Util.formatNumber(rightNow.get(Calendar.SECOND),2));
        logEntry.append("s ");
        logEntry.append(Util.formatNumber(rightNow.get(Calendar.MILLISECOND),3));
        logEntry.append("ms ");
        logEntry.append(separator);
        logEntry.append(" ");
        logEntry.append(s);
        
        return logEntry.toString();
    } 

    //===================================================================== 
 
    public static String add(String s,String divider) {        
    	
    	String result=createEntry(s,divider);
        try {                	
            messageQueue.put(result);
        }
        catch (InterruptedException ex){
        	ex.printStackTrace();
        }
        return result;
    } 

    //=====================================================================      
    
//=====================================================================
 
} //End of Class

//=====================================================================
//END OF FILE Logger.java