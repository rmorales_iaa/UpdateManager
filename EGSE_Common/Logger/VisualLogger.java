//===================================================================== 
//  Description:Visual Logger implementation
//  File:Logger.java
//  Package:Logger
//  Author:Rafael Morales Mu�oz
//  Date:04 January 2008
//  Version:0.1
//  History:none
//=====================================================================

//=====================================================================
//Package Name
//=====================================================================

package EGSE_Common.Logger;

//=====================================================================
//Imports
//=====================================================================

//User imports
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

import EGSE_Common.GUI.GUI_Util;
import EGSE_Common.Util.CommonConstant;

//=====================================================================
//Class Declaration
//=====================================================================

public class VisualLogger extends Logger {

//=====================================================================                                                                                        
	//Private vars

	//Visual vars
	private JTextPane textPanel = new JTextPane();    
	private static JScrollBar verticalScrollBar;
	
	//Visual Limits
	private static int loggerMaxLineShow=255;
	private static int numberOfLineLogger=0;
	private static ArrayList<Integer> endOfLinePositionQueue=new ArrayList<Integer>();        
	//Styled document
	private static StyledDocument doc; 
	
	//Divider font
	private static HashMap<String,Style> dividerFontTypeMap=new HashMap<String,Style>();
		
//=====================================================================                                                                                        
//Public constants

    //=====================================================================
	
    public void initializeAndStart(JScrollPane scrollPanel,String path) {

    	super.initializeAndStart(path);

    	textPanel = new JTextPane();
    	textPanel.setEditable(false);    	
 
    	//Get the vertical bar
    	verticalScrollBar=scrollPanel.getVerticalScrollBar();

    	//Add the textPanel to the scroll
    	scrollPanel.getViewport().add(textPanel,null);

    	//Set the styled doc
    	doc=textPanel.getStyledDocument();
    	GUI_Util.initDocument(doc);
    	createDividerFontType();
    	
    	textPanel.setFont(GUI_Util.TEXT_FONT);
    	textPanel.setForeground(GUI_Util.COLOR_DEFAULT_FOREGROUND);
    	textPanel.setBackground(GUI_Util.COLOR_DEFAULT_BACKGROUND);
    }

    //=====================================================================
    
    public static void addErrorToLog(String s) {        
        
        add(s,ERROR_DIVIDER);
    } 
    
    //=====================================================================                      

    public static void addMessageToLogAndWindow(String s) {        
     
        add(s,DEFAULT_DIVIDER);
        JOptionPane.showMessageDialog(null,s,"Information",
                                      JOptionPane.INFORMATION_MESSAGE); 
    } 
    
    //=====================================================================                      

    public static void addMessageToLogAndWindow(String s,String divider) {        
     
        add(s,divider);
        JOptionPane.showMessageDialog(null,s,"Information",
                                      JOptionPane.INFORMATION_MESSAGE); 
    } 
    
    //=====================================================================                      

    public static void addErrorToLogAndWindow(String s) {        
     
        add(s,ERROR_DIVIDER);
        JOptionPane.showMessageDialog(null,s,"ERROR",JOptionPane.ERROR_MESSAGE);               
    } 

    
    //=====================================================================                      

    public static void addErrorToLogAndWindow(String title,String s) {        
     
        add(s,ERROR_DIVIDER);
        JOptionPane.showMessageDialog(null,s,title,JOptionPane.ERROR_MESSAGE);               
    } 
    
    //=====================================================================                      

    public static void addWarningToLog(String s) {        
     
        add(s,WARNING_DIVIDER);
    } 

    //=====================================================================      
    
    public static synchronized String add(String s,String divider) {
        
    	s=Logger.add(s,divider); //Add to the text log
    	
    	//Use the style according with the extension
    	Style style=dividerFontTypeMap.get(divider);
    	if (style==null) style=GUI_Util.STYLE_DEFAULT;
    	
    	//Add the message into the window
    	String lineToShow="";
    	if(!endOfLinePositionQueue.isEmpty()) lineToShow+=CommonConstant.NEW_LINE;
    	lineToShow+=s;
    	try { 
        	doc.insertString(doc.getLength(),lineToShow,style);
    	} 
        catch (BadLocationException e) {
            VisualLogger.addErrorToLogAndWindow("Failed to append text to the window." + e.getMessage());
        }
            
        //If we reached the maximum number of lines then remove first one
        if( ++numberOfLineLogger >= loggerMaxLineShow){
        	try { 
        		int size=endOfLinePositionQueue.get(0);
        		int maxSize=doc.getLength();
        		if (doc.getLength()<size) size=maxSize;
        		doc.remove(0,size);
        		endOfLinePositionQueue.remove(0);
        	} 
            catch (BadLocationException e) {
            	addErrorToLogAndWindow("Failed to delete the first line." + e.getMessage());
            }
	        --numberOfLineLogger;
	    }
        
        //Calculate the line size
        int size=lineToShow.length();
        if(endOfLinePositionQueue.isEmpty()) size+=CommonConstant.NEW_LINE.length();
        
        //Push the size of the current line
        endOfLinePositionQueue.add(size);
        
        //Go to last line
        gotoLastLine();

        return s;
    }
    
    //=====================================================================
    
	public static void setLoggerMaxLineShow(int loggerMaxLineShow) {
		
		VisualLogger.loggerMaxLineShow = loggerMaxLineShow;
	}    

    //=====================================================================
	
	public static void gotoLastLine(){
		
   	    SwingUtilities.invokeLater(new Runnable()
   	    	{
   	    		public void run()
   	    		{        	
   	    			verticalScrollBar.setValue(verticalScrollBar.getMaximum());
   	    		}
   	    	});	    
	}
  
	
    //=====================================================================
	
	private static void createDividerFontType() {
		
		if (dividerFontTypeMap.size()>0) return;
		
		dividerFontTypeMap.put(DEFAULT_DIVIDER,GUI_Util.STYLE_DEFAULT);
		dividerFontTypeMap.put(WARNING_DIVIDER,GUI_Util.STYLE_MONOSPACED_WARNING);
		dividerFontTypeMap.put(ERROR_DIVIDER,GUI_Util.STYLE_MONOSPACED_ERROR);
	}    

    //=====================================================================
   
//=====================================================================
 
} //End of Class

//=====================================================================
//END OF FILE ViusalLogger.java