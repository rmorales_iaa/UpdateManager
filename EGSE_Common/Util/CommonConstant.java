// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 29/03/2011 14:12:55
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   CommonConstant.java

package EGSE_Common.Util;


public interface CommonConstant
{

    public static final String MONTH[] = {
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", 
        "November", "December"
    };
    public static final String DAY[] = {
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    };
    public static final String DOT = ".";
    public static final String DASH = "_";
    public static final String LOG_EXTENSION = "txt";
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String pathDivider = "/";

}