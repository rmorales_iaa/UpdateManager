//=====================================================================
//  Description:Util functions
//  File:Util.java
//  Package:EGSE_Common.Util
//  Author:Rafael Morales Mu�oz
//  Date:15 April 2007
//  Version:0.1
//  History:none
//=====================================================================

//=====================================================================
//Package Name
//=====================================================================

package EGSE_Common.Util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import EGSE_Common.Logger.Logger;
import EGSE_Common.Logger.VisualLogger;

//=====================================================================
//Imports
//=====================================================================

//User imports

//=====================================================================
 
public class Util implements CommonConstant{

//===================================================================== 
//Private variables
	
	//=====================================================================
	
	public static String getCurrentPath(){
		
		return new File(".").getAbsolutePath();
	}

    //===================================================================== 
	
    public static final String formatNumber(int n,int maxSize) {   
        
        String s= String.valueOf(n);                
        int max=maxSize-s.length();
        for (int i=0;i<max;++i)
            s="0"+s;
        return s;  
    }
    
    //=====================================================================
    
    public static int writeString(BufferedWriter f,String s){
    
        if (s==null) return 1;
        try {
            f.write(s,0,s.length());
            f.flush();
        }
        catch (IOException e)
        {
            VisualLogger.add(e.getMessage(),Logger.DEFAULT_DIVIDER);
            VisualLogger.addMessageToLogAndWindow("Can not put the string:'" + s + 
                             "' into the output file",Logger.DEFAULT_DIVIDER);  
            return -1;
        }          
        return 1;
    }
    
    
    //=====================================================================    
    
    public static String replaceSlashByLinuxSlash(String s){
    	
        s=s.replace("\\","/");
        return s;
    }
    //===================================================================== 
	//=====================================================================              
} //End of Class

//=====================================================================
//END OF FILE "Util.java"