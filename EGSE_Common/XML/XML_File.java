//=====================================================================
//  Description:Load a XML file. Can  put it in a visual jtree model
//  File:XML_File.java
//  Package:EGSE_Common.XML
//  Author:Rafael Morales Mu�oz
//  Date:25 August 2007
//  Version:0.1
//  History:none
//=====================================================================

//=====================================================================
//Package Name
//=====================================================================

package EGSE_Common.XML;

//=====================================================================
//Imports
//=====================================================================

//User imports

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import EGSE_Common.DataHandler.TreeSortModel;
import EGSE_Common.DataHandler.TreeXML_Node;
import EGSE_Common.Logger.Logger;
import EGSE_Common.Logger.VisualLogger;
import EGSE_Common.Util.Util;

//=====================================================================
 
public class XML_File {

//===================================================================== 
//Constants
    
//===================================================================== 
//Private variables
    
	//Filename
	private String filename;
	
    //List
    private DefaultListModel listDataModel;    
    private String listNodeName;
    
    //Tree
    private Document XML_Doc;            
    private TreeSortModel treeSortModel;
   
    //XML root
    private Node xmlRootNode;    
    
    //Max level
    private int maxNodeLevel;
    
    //Storage
    private Map<String, String> dataStorage=new HashMap<String, String>();
    private Map<String, Node> dataStorageXML_Node=new HashMap<String, Node>();
    
    //Xpath
    private XPathFactory xPathfactory = XPathFactory.newInstance();
    private XPath xPath = xPathfactory.newXPath();
        
  //===================================================================== 
    
    public XML_File(String filename,boolean checkDTD,int max,TreeSortModel m,int result[]){

        maxNodeLevel=max;
        treeSortModel=m;
        listDataModel=null;
        listNodeName=null;
        
        result[0]=constructorCommonPart(filename,checkDTD,m!=null);
    }

    //===================================================================== 

    private int constructorCommonPart(String fileName,boolean checkDTD,boolean buildVisualInterface){
        
    	this.filename=fileName;
    	
        //Parse the XML file
        return parseXML_File(fileName,checkDTD,buildVisualInterface);
    }

    //===================================================================== 
    
    private int parseXML_File(String fileName,boolean checkDTD,boolean buildVisualInterface){
        
        int result=1;
        
        //Set the correct slash
        filename=Util.replaceSlashByLinuxSlash(filename);
        
        //Open the file
        File f = new File(fileName);
    	        
        try {
            //XML
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(checkDTD);            
            factory.setIgnoringComments(true);                        
            DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new org.xml.sax.ErrorHandler() {
                // ignore fatal errors (an exception is guaranteed)
                public void fatalError(SAXParseException exception)
                    throws SAXException {
                }
                 // treat validation errors as fatal
                public void error(SAXParseException e)
                    throws SAXParseException{
                        throw e;
                    }
                 // dump warnings too
                public void warning(SAXParseException err)
                    throws SAXParseException{
                        VisualLogger.addErrorToLogAndWindow("Warning"+ ", line " + err.getLineNumber()+ ", uri " + err.getSystemId());
                        VisualLogger.addErrorToLogAndWindow("   " + err.getMessage());
                    }
                }); 
            
            XML_Doc = builder.parse(f);                                     
        } catch (SAXException sxe) {
            // Error generated during parsing
            Exception  x = sxe;
            VisualLogger.addErrorToLogAndWindow(x.getMessage());
            if (sxe.getException() != null)
                x = sxe.getException();
            x.printStackTrace();
            result=-10;
         } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            VisualLogger.addErrorToLogAndWindow(pce.getMessage());
            pce.printStackTrace();
            result=-11;
         } catch (IOException ioe) {
            // I/O error
            VisualLogger.addErrorToLogAndWindow(ioe.getMessage());
            ioe.printStackTrace();
            result=-12;
        }
            
        //Check the result
        if (result<0){
            XML_Doc=null;
            XML_Doc=null;
            return result;
        }
        else xmlRootNode=getXML_Doc().getDocumentElement();        
        
        //Create the visual interface
        if (buildVisualInterface) {
            if (listDataModel!=null) parseXML_NodeStoreList(xmlRootNode,1);
            else parseXML_NodeStoreTree(xmlRootNode,null,1);    
        }
        return 1;
   }

   //=====================================================================
    
    private void parseXML_NodeStoreList(Node xmlNode,int level){
        
        //Process its components
        NodeList childList=xmlNode.getChildNodes();
        int max=childList.getLength();
        for(int i=0;i<max;++i){
                            
            Node child=childList.item(i);             
            if (child.getNodeType()!=Node.ELEMENT_NODE) continue; //Only nodes     

            createListItem(child);
            
            if (maxNodeLevel>0) 
                if (level>=maxNodeLevel) continue; //No more children
            
            //Parse its children
            parseXML_NodeStoreList(child,level+1);
        }               
    }

    //=====================================================================
    
    private void parseXML_NodeStoreTree(Node xmlNode,TreeXML_Node parentTreeNode,int level){
        
        if (parentTreeNode==null) //Detect the root
            if (xmlNode.getNodeType()==Node.ELEMENT_NODE) {                
                TreeXML_Node newTreeNode=createTreeNode(xmlNode);
                treeSortModel.setRoot(newTreeNode);
                parentTreeNode=newTreeNode;
            }
                            
        //Process its components
        NodeList childList=xmlNode.getChildNodes();
        int max=childList.getLength();
        for(int i=0;i<max;++i){
                            
            Node child=childList.item(i);             
            if (child.getNodeType()!=Node.ELEMENT_NODE) continue; //Only nodes     
                       
            TreeXML_Node newTreeNode=createTreeNode(child);   
            
            //Add the new node                 
            treeSortModel.insertNodeInto(newTreeNode,parentTreeNode);
            
            if (level==1){ //Add to storage only the nodes of level 1
            	NamedNodeMap attributeList=child.getAttributes();
            	if (attributeList.getLength()>0)
            		//Add to storage
            		dataStorageXML_Node.put(((Attr)attributeList.item(0)).getValue(),child);
            }
            
            if (maxNodeLevel>0) 
                if (level>=maxNodeLevel) continue; //No more children
                                                       
            //Parse its children
            parseXML_NodeStoreTree(child,newTreeNode,level+1);
        }        
    }
    
    //===================================================================== 
   
    private TreeXML_Node createTreeNode(Node xmlNode){
    		
        if (!xmlNode.hasAttributes()) {
            String name=xmlNode.getNodeName();
            String value=xmlNode.getTextContent();
            if (value!=null) name+="="+value;
            return new TreeXML_Node(name); 
        }
        
        //Get the attributes
        StringBuffer buf = new StringBuffer();
        NamedNodeMap attributeList=xmlNode.getAttributes();
        int max=attributeList.getLength();
        for(int i=0;i<max;++i){
            Attr attribute = (Attr)attributeList.item(i);
            buf.append("'"+attribute.getValue()+"'"); 
            if (i!=(max-1)) buf.append(";");                    
        }
        
        return new TreeXML_Node(xmlNode.getNodeName()+buf.toString()); 
    }    
    
    //===================================================================== 
    
     private void createListItem(Node xmlNode){
        
        NamedNodeMap attributeList=xmlNode.getAttributes();       
        String parentName=((Attr)attributeList.item(0)).getValue();               

        //Find the implementation
        String childName="";
        NodeList childList=xmlNode.getChildNodes();
        int max=childList.getLength();
        for(int i=0;i<max;++i){                            
            Node child=childList.item(i);             
            if (child.getNodeType()!=Node.ELEMENT_NODE) continue; //Only nodes     
            if (child.getNodeName().equals(listNodeName)){
                childName=child.getTextContent();                
                break;
            }
        }
       
       //Add to the visual data model        
        listDataModel.addElement(parentName);
        
        //Add to storage
        dataStorage.put(parentName,childName);        
        dataStorageXML_Node.put(parentName,xmlNode);            
    }    
     
    
   //===================================================================== 
     
     public String executeXPathGetFirstValue(String xPathSentence){
     
    	 NodeList nodeList=executeXPath(xPathSentence);
    	 if (nodeList==null) return null;
    	 int max=nodeList.getLength();
    	 for(int i=0;i<max;++i){
    		 Node child=nodeList.item(i); 
    		 if (child.getNodeType()!=Node.ELEMENT_NODE) continue; //Only nodes    		          
    		 else return getNodeValue(child);
    	 }
    	 return null;
     }
     
     //===================================================================== 
     
    public NodeList executeXPath(String xPathSentence){
        
    	NodeList resultNodeList=null;
    	
       	try{
       		XPathExpression xPathExpression=xPath.compile(xPathSentence);
       		resultNodeList=(NodeList)xPathExpression.evaluate(XML_Doc,XPathConstants.NODESET);
       	}
       	catch(XPathExpressionException ex ){
       	
       		Logger.add("Error processing the xPath sentence '"+xPathSentence+"'",Logger.XPATH_DIVIDER);
       		Logger.add(ex.getMessage(),Logger.XPATH_DIVIDER);
       		return null;
       	}
        
        return resultNodeList;
    }   
    
    //=====================================================================

    
    public Node getXML_RootNode() {
        return xmlRootNode;
    }
    
    //===================================================================== 

    public Document getXML_Doc() {
        return XML_Doc;
    }
    
    //===================================================================== 

    public Map<String, String> getDataStorage() {
        return dataStorage;
    }

    //===================================================================== 
    
    public static String getAttribute(Node xmlNode,String attributeName){    	
    	    	
    	String s=((Element)xmlNode).getAttribute(attributeName);
    	if (s!=null) s=s.trim();
    	return s;
    }

    //===================================================================== 
    
    public static String getNodeValue(Node xmlNode){
        	
    	if (!((Element)xmlNode).hasChildNodes()) return null;
    	else return ((Element)xmlNode).getChildNodes().item(0).getNodeValue();
    }
    
    //=====================================================================
    
    public String getFilename(){
    	
    	return filename;
    }
    
    //=====================================================================
      
} //End of Class

//=====================================================================
//END OF FILE "XML_File.java"