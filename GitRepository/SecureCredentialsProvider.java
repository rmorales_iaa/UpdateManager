//=====================================================================
//It provides the credentials to access to ssh server
//=====================================================================

package GitRepository;

//=====================================================================

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;

//=====================================================================

public class SecureCredentialsProvider extends CredentialsProvider {
	
	  public interface Factory {
	    SecureCredentialsProvider create(String remoteName);
	  }

	//=====================================================================
	  
	  private final String cfgUser;
	  private final String cfgPass;


	//=====================================================================
	  
	  public SecureCredentialsProvider(String user,String pass) {
	    cfgUser = user;
	    cfgPass = pass;
	  }

	  //=====================================================================
	  
	  public boolean isInteractive() {
	    return false;
	  }

	  //=====================================================================
	  
	  public boolean supports(CredentialItem... items) {
	    for (CredentialItem i : items) {
	      if (i instanceof CredentialItem.Username) {
	        continue;
	      } else if (i instanceof CredentialItem.Password) {
	        continue;
	      } else {
	        return false;
	      }
	    }
	    return true;
	  }

	  //=====================================================================
	  
	  public boolean get(URIish uri, CredentialItem... items)
	      throws UnsupportedCredentialItem {
	    String username = uri.getUser();
	    if (username == null) {
	      username = cfgUser;
	    }
	    if (username == null) {
	      return false;
	    }

	    String password = uri.getPass();
	    if (password == null) {
	      password = cfgPass;
	    }
	    if (password == null) {
	      return false;
	    }

	    for (CredentialItem i : items) {
	      if (i instanceof CredentialItem.Username) {
	        ((CredentialItem.Username) i).setValue(username);
	      } 
	      else 
	    	  if (i instanceof CredentialItem.Password) {
	    		  ((CredentialItem.Password) i).setValue(password.toCharArray());
	    	  } 
	    	  else {
	    		  if (i instanceof CredentialItem.StringType) {
		    		  ((CredentialItem.StringType) i).setValue(password);
		    	  } 
	    		  else
	    			  throw new UnsupportedCredentialItem(uri, i.getPromptText());
	    	  }
	    }
	    return true;
	  }

	  //=====================================================================
	  
	}
//=====================================================================
//=====================================================================
//End of file 'SecureCredentialsProvider.java'
//=====================================================================
