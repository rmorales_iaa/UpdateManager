//=====================================================================

package MyProgressMonitor;

//=====================================================================

import org.eclipse.jgit.lib.ProgressMonitor;

import EGSE_Common.Logger.VisualLogger;

//=====================================================================

public class MyProgressMonitor implements ProgressMonitor {

	//=====================================================================
	
	private String monitorName; 
	
	//=====================================================================
	
	private static final String LOGGER_DIVIDER_UPDATE="[UPD]";
	
	//=====================================================================
	
	public MyProgressMonitor(String name){
	
		monitorName=name;
	}
	
	//=====================================================================
	
	public void beginTask(String title, int totalWork) {
		
		VisualLogger.add("MyProgressMonitor::"+monitorName+".Started task name '"+title+"' total work:"+totalWork,LOGGER_DIVIDER_UPDATE);	
	}

	//=====================================================================
	
	public void endTask() {
		
		VisualLogger.add("MyProgressMonitor::"+monitorName+".Work unit end",LOGGER_DIVIDER_UPDATE);			
	}

	//=====================================================================
	
	public boolean isCancelled() {
			
		return false;
	}

	//=====================================================================	
	
	public void start(int totalTask) {
		
		VisualLogger.add("MyProgressMonitor::"+monitorName+".Started with: "+totalTask+" work units",LOGGER_DIVIDER_UPDATE);		
	}

	//=====================================================================	
	
	public void update(int completed) {
		
		VisualLogger.add("MyProgressMonitor::"+monitorName+".Number of work units completed since the last call:"+completed,LOGGER_DIVIDER_UPDATE);			
	}
	
	//=====================================================================
}
//=====================================================================
//=====================================================================
//End of task 'MyProgressMonitor.java'
//=====================================================================
