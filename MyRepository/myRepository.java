//=====================================================================

package MyRepository;

//=====================================================================

//System imports
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.CanceledException;
import org.eclipse.jgit.api.errors.DetachedHeadException;
import org.eclipse.jgit.api.errors.InvalidConfigurationException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.WrongRepositoryStateException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.tmatesoft.svn.core.*;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


//User imports

import EGSE_Common.Logger.VisualLogger;
import EGSE_Common.Util.Util;
import EGSE_Common.XML.XML_File;
import GitRepository.SecureCredentialsProvider;
import MyProgressMonitor.MyProgressMonitor;
import SVN_Repository.SVN_Handler.SVN_CommitEventHandler;
import SVN_Repository.SVN_Handler.SVN_StatusHandler;
import SVN_Repository.SVN_Handler.SVN_UpdateEventHandler;
import SVN_Repository.SVN_Handler.SVN_WC_EventHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

//=====================================================================

public class myRepository implements Runnable
{
	
	//Constants	
	private static final String LOGGER_DIVIDER_UPDATE="[UPD]";

	//Variables
	
	//Visual variables
    private JScrollPane jScrollPaneLogger;
    private JFrame frameMain;
    
    //Path
    private static String currentPath;
    
    //XML
    private static final String xmlPath = "Input/XML/UpdateManager.xml";
    private static final String loggerPath = "Output/Logger";
        
    //Changed file list
    public static ArrayList<Object> changedFileList = new ArrayList<Object>();
    
    //Common repositories
    private static String repositoryURL;
    private static String repositoryName;
    private static String repositoryUserName;
    private static String repositoryPassword;
    private static String repositoryLocalPathToUpdate;
    
    //Git
    private static String gitPath;
    private static Git myGit;
    private static Repository repositoryGit;
    private static String GIT_REPOSITORY_DIRECTORY_NAME="/.git/";
    
    //Subversion
    private static String subversionUsername;
    private static SVNClientManager ourClientManager;
    private static SVNRepository repository;
    private static ISVNOptions SVN_options = SVNWCUtil.createDefaultOptions(true);
    private static ISVNEventHandler myCommitEventHandler = new SVN_CommitEventHandler();
    private static SVN_StatusHandler myStatusEventHandler;
    private static ISVNEventHandler myUpdateEventHandler = new SVN_UpdateEventHandler();
    private static ISVNEventHandler myWC_EventHandler = new SVN_WC_EventHandler();
    private static XML_File xmlFile = null;
    
    private static int error=0;

//=====================================================================
	
    public myRepository(JScrollPane jScrollPaneLogger, JFrame frameMain)
    {
        this.jScrollPaneLogger = jScrollPaneLogger;
        this.frameMain = frameMain;
        (new Thread(this)).start();
    }

//=====================================================================
    
    public void run()
    {
        if(!initialization()) return;
        processRepositoryList();
        
        VisualLogger.addWarningToLog("Update Manager ends.");
        System.exit(1);
    }

 //=====================================================================
    
    private boolean initialization()
    {
    	//Logger
        initLogger();                
        
        //XML
        int result []={-1};
        xmlInitialization(result);        
        if (result[0]<0) return false;
        
        //Application initialization
        if(!applicationInitialization()) return false;
        
        error=0;
        return true;
   }
    
 //=====================================================================
    
    private void initLogger()
    {
        currentPath = Util.getCurrentPath();
        currentPath = currentPath.substring(0, currentPath.length() - 1);
        String path=(new StringBuilder()).append(currentPath).append(loggerPath).toString();
        
        path=Util.replaceSlashByLinuxSlash(path);

        (new VisualLogger()).initializeAndStart(jScrollPaneLogger,path);

        VisualLogger.setLoggerMaxLineShow(100);
        VisualLogger.gotoLastLine();
    }
    
 //=====================================================================
    
    private void xmlInitialization(int result [])
    {    	
        xmlFile = new XML_File((new StringBuilder()).append(currentPath).append(xmlPath).toString(), false, -1, null,result);
    }

    //=====================================================================

    private String getXML_Attribute(Node node,String attributeName)
    {   
    	return XML_File.getAttribute(node,attributeName);	
    }

  //=====================================================================
    
    private String getXML_Value(String xpath,String errorMessage)
    {   
        String s = xmlFile.executeXPathGetFirstValue(xpath);
        if(s == null){
            VisualLogger.addErrorToLogAndWindow((new StringBuilder()).append("Can not get from the xml file the ").append(errorMessage).toString());
            ++error;
        }
        return s;
    }
    
   //=====================================================================
    
    private static String getXML_Value(Node node,String nodeName,String errorMessage)
    {   
    	NodeList nodeList=node.getChildNodes();
    	
    	int max=nodeList.getLength();    	
    	for(int i=0;i<max;++i){    		
    		Node child=nodeList.item(i); 
   		 	if (child.getNodeType()!=Node.ELEMENT_NODE) continue; //Only nodes    		          
   		 	if (child.getNodeName().equals(nodeName)) return XML_File.getNodeValue(child);   		     			
    	}
        VisualLogger.addErrorToLogAndWindow((new StringBuilder()).append("Can not get from the xml file the ").append(errorMessage).toString());
        ++error;
        return null;
    }
    
    //=====================================================================
    
    private boolean applicationInitialization()
    {
        String s = getXML_ApplicationTitle();
        if(s != null)
            frameMain.setTitle(s);
        else
            return false;
        return true;
    }
    
//=====================================================================
    
    private String getXML_ApplicationTitle()
    {
        return getXML_Value("UpdateManager/Application/title", "application title");
    }
    
//=====================================================================
    	
   private boolean processRepositoryList(){
	   
	   processSubversionRepositoryList();
	   processGitRepositoryList();
	   
       return true;
   }

 //=====================================================================
	
   private boolean processGitRepositoryList(){
    	
    	NodeList nodeList=xmlFile.executeXPath("UpdateManager/Git");
	 	if (nodeList==null) return true;
	 	
	 	int max=nodeList.getLength();
	 	for(int i=0;i<max;++i){	 	
	 		Node node=nodeList.item(i);
	 		if (!processGitRepository(node)) return false;
	 	
	        updateFromGitRepository();        
	 	}
	 	return true;
   }
   
   //=====================================================================
   
   private boolean processGitRepository(Node node){
	  
 	    boolean ok=true;;
    	    	
    	//Git path
    	if(getGitPath(node) == null) return false;
    	
        //Repository name
	    if(getRepositoryName(node) == null) return false;
	      	
	    //Repository URL  	
	    if(getRepositoryURL(node) == null)return false;
	    
	    //Repository user name
	    if(getRepositoryUserName(node) == null) return false;
	    
	    //Repository password
	    if(getRepositoryPassword(node) == null) return false;
    	  
    	//Repository local path to update  
	    ok=ok&&getRepositoryLocalPathToUpdate(node) != null;
    	return ok;
  }

   //=====================================================================
   
	private boolean updateFromGitRepository() {
	   
		VisualLogger.add("Setting up the repository '" + repositoryName+"'",LOGGER_DIVIDER_UPDATE);
	       
    	//open the local repository
    	FileRepositoryBuilder builder = new FileRepositoryBuilder();
    	try {
    		//check git directory name
    		if (!repositoryLocalPathToUpdate.endsWith(GIT_REPOSITORY_DIRECTORY_NAME)) repositoryLocalPathToUpdate+=GIT_REPOSITORY_DIRECTORY_NAME;
    			
    		//set directory
    		repositoryGit = builder.setGitDir(new File(repositoryLocalPathToUpdate))
			  .readEnvironment() // scan environment GIT_* variables
			  .findGitDir() // scan up the file system tree
			  .build();
		} 
    	catch (IOException e) {
			VisualLogger.add("myRepository::updateFromGitRepository().Error opening git repository."+e.toString(),LOGGER_DIVIDER_UPDATE);		
			return false;
		}   	
		
    	VisualLogger.add("Logged in repository '" + repositoryName+"' getting last changes",LOGGER_DIVIDER_UPDATE);
    	
    	//create repository
    	myGit = new Git(repositoryGit);
    	
    	//rafa:activate when clone works
    	//if (!buildLocalGitRepositoryFromScracth()) return false;
    	//rafa:end
    	
    	//apply pull    	
    	if (!gitPull()) return false;
    	
    	//get status
    	return gitStatus();
	}

	//=====================================================================
	   
	private boolean buildLocalGitRepositoryFromScracth() {
						
		boolean exists = (new File(repositoryLocalPathToUpdate)).exists();		
		if (exists) {
			VisualLogger.add("Local repository '" + repositoryName+"' exists",LOGGER_DIVIDER_UPDATE);
			return true;
		}
			
		//clone repository		
		return gitClone();
	}
		
	//=====================================================================
	
	private boolean gitClone(){
    	
		VisualLogger.add("Clonning remote repository '" + repositoryURL+"'",LOGGER_DIVIDER_UPDATE);
		
    	//execute check out
		try {
			CloneCommand command=myGit.cloneRepository();
		
			//configure command
			command.setCloneAllBranches(true);
			command.setBare(true);
			command.setRemote(repositoryURL);
			command.setDirectory(new File(repositoryLocalPathToUpdate));
			command.setURI(repositoryURL);
			command.setProgressMonitor(new MyProgressMonitor("gitClone"));
			command.setNoCheckout(false);
			
	    	//build the credential for ssh connection and set it to pull
	    	SecureCredentialsProvider credential=new SecureCredentialsProvider(repositoryUserName,repositoryPassword);
	    	command.setCredentialsProvider(credential);

	    	//execute command
			command.call();
		} catch (JGitInternalException e) {
			VisualLogger.add("myRepository::gitClone().Internal exception."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		}
		
		VisualLogger.add("Cloned remote repository '" + repositoryURL+"' sucessfully",LOGGER_DIVIDER_UPDATE);
		
		return true;
	}
	
    //=====================================================================
    	
    private boolean gitStatus(){
    		    	
    	VisualLogger.add("Getting status of local repository '" + repositoryName+"'",LOGGER_DIVIDER_UPDATE);
    	
		Status status;
		try {
			status = myGit.status().call();
		} catch (NoWorkTreeException e) {
			VisualLogger.add("myRepository::gitStatus().Wrong repository state."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (IOException e) {
			VisualLogger.add("myRepository::gitStatus().Wrong repository state."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		}

		boolean someChange=false;
		
		//added
		for (String item:status.getModified()){
			someChange=true;
			VisualLogger.add("Modified file: "+item,LOGGER_DIVIDER_UPDATE);			
		}
		
		//changed
		for (String item:status.getChanged()){
			someChange=true;
			VisualLogger.add("Changed file: "+item,LOGGER_DIVIDER_UPDATE);
		}
		
		//removed
		for (String item:status.getRemoved()){
			someChange=true;
			VisualLogger.add("Removed file: "+item,LOGGER_DIVIDER_UPDATE);
		}			
		
		//conflicting
		for (String item:status.getConflicting()){
			someChange=true;
			VisualLogger.add("Conflicting file: "+item,LOGGER_DIVIDER_UPDATE);
		}

		//missing
		for (String item:status.getMissing()){
			someChange=true;
			VisualLogger.add("Missing file: "+item,LOGGER_DIVIDER_UPDATE);
			VisualLogger.add("Getting local copy: "+item,LOGGER_DIVIDER_UPDATE);
			if (!gitCheckout(item)) return false;
		}
		
		//untracked
		for (String item:status.getUntracked()){
			someChange=true;
			VisualLogger.add("Untracked file: "+item,LOGGER_DIVIDER_UPDATE);
			VisualLogger.add("Getting local copy: "+item,LOGGER_DIVIDER_UPDATE);
			if (!gitCheckout(item)) return false;
		}
		
		if (!someChange)
			VisualLogger.add("No changes detected in the server",LOGGER_DIVIDER_UPDATE);
		
		VisualLogger.add("Status of local repository '" + repositoryName+"' has benn obtained witn no errors",LOGGER_DIVIDER_UPDATE);
		
		return true;     
   }


	//=====================================================================
	
	private boolean gitCheckout(String fileName){
    	
		VisualLogger.add("Getting file '"+fileName+"' from local repository",LOGGER_DIVIDER_UPDATE);
	
		/*RevWalk rw = new RevWalk(repositoryGit);
		ObjectId objHead = repositoryGit.resolve("HEAD");				
	    RevCommit commitHead = rw.parseCommit(objHead);        		        	    
	        
	    RevTree tree=commitHead.getTree();
	    */
		
		/*
    	//execute check out
		try {
			CheckoutCommand checkout=myGit.checkout();
			checkout.setForce(true);
			checkout.setName(fileName);
			checkout.call();
		} catch (JGitInternalException e) {
			VisualLogger.add("myRepository::gitCheckout().Internal exception."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (RefAlreadyExistsException e) {
			VisualLogger.add("myRepository::gitCheckout().Reference already exists."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (RefNotFoundException e) {
			VisualLogger.add("myRepository::gitCheckout().Reference not fonud."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (InvalidRefNameException e) {
			VisualLogger.add("myRepository::gitCheckout().Invalid reference name."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		}*/
		
		VisualLogger.add("Checkout of local repository '" + repositoryName+"' sucessfully",LOGGER_DIVIDER_UPDATE);
		
		return true;
	}
	
	//=====================================================================
	
	private boolean gitPull(){
		    	
		VisualLogger.add("Getting last changes of remote repository '" + repositoryURL+"'",LOGGER_DIVIDER_UPDATE);
		
    	//execute pull
		try {
	    	PullCommand command=myGit.pull();
	    	
	    	//build the credential for ssh connection and set it to pull
	    	SecureCredentialsProvider credential=new SecureCredentialsProvider(repositoryUserName,repositoryPassword);
			command.setCredentialsProvider(credential);
			
			//set progress monitor
			command.setProgressMonitor(new MyProgressMonitor("gitPull"));
			
			//execute command
			command.call();
		} catch (WrongRepositoryStateException e) {
			VisualLogger.add("myRepository::gitPull().Wrong repository state."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (InvalidConfigurationException e) {
			VisualLogger.add("myRepository::gitPull(). Invalid configuration."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (DetachedHeadException e) {
			VisualLogger.add("myRepository::gitPull().Detached head."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (InvalidRemoteException e) {
			VisualLogger.add("myRepository::gitPull().Invalid remote server."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (CanceledException e) {
			VisualLogger.add("myRepository::gitPull().Canceled."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (RefNotFoundException e) {
			VisualLogger.add("myRepository::gitPull().Ref not found."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		} catch (NoHeadException e) {
			VisualLogger.add("myRepository::gitPull().No head exception."+e.toString(),LOGGER_DIVIDER_UPDATE);
			return false;
		}
		
		VisualLogger.add("Pulling remote repository '" + repositoryURL+"' sucessfully",LOGGER_DIVIDER_UPDATE);
		
		return true;
	}
	
	//=====================================================================
		
    private String getGitPath(Node node)
    {
        return gitPath = getXML_Value(node,"gitPath","Git path");
    }
    
   //=====================================================================
	
   private boolean processSubversionRepositoryList(){
    	
    	NodeList nodeList=xmlFile.executeXPath("UpdateManager/SubVersion");
	 	if (nodeList==null) return true;
	 	
	 	int max=nodeList.getLength();
	 	for(int i=0;i<max;++i){	 	
	 		Node node=nodeList.item(i);
	 		if (!processSubversionRepository(node)) return false;
	        setupSubversionRepository();
	        if(checkConnectionWithSubversionRespository()) updateFromSubversionRepository();        
	 	}
	 	return true;
   }
	 
	 
   //=====================================================================
   
  private boolean processSubversionRepository(Node node){
	  
    boolean ok=true;;
    	    	
   	//Repository name
  	if(getRepositoryName(node) == null) return false;
  	
  	//Repository urm
    if(getRepositoryURL(node) == null)return false;
    
    //Repository user name
    if(getRepositoryUserName(node) == null) return false;
    
    //Repository password
    if(getRepositoryPassword(node) == null) return false;
    	
    //Repository local path to update	
    ok=ok&&getRepositoryLocalPathToUpdate(node) != null;
    	
    return ok;
  }
    
 //=====================================================================
        
    private String getRepositoryName(Node node){
    	
    	return repositoryName = getXML_Attribute(node,"name");
    }
        
 //=====================================================================
    
    private String getRepositoryURL(Node node)
    {
        return repositoryURL = getXML_Value(node,"url","repository URL");
    }
    
//=====================================================================
    
    private String getRepositoryUserName(Node node)
    {
        return repositoryUserName = getXML_Value(node,"userName","repository userName");
    }
    
//=====================================================================
    
    private String getRepositoryPassword(Node node)
    {
        return repositoryPassword = getXML_Value(node,"password", "repository password");
    }
    
//=====================================================================
    
    private String getRepositoryLocalPathToUpdate(Node node)
    {
        return repositoryLocalPathToUpdate = getXML_Value(node,"localPathToUpdate","repository local path to update");
    }
    
//=====================================================================
    
    private boolean setupSubversionRepository()
    {
        VisualLogger.addWarningToLog("Setting up the repository " + repositoryName);
        DAVRepositoryFactory.setup();
        SVNRepositoryFactoryImpl.setup();
        FSRepositoryFactory.setup();
        org.tmatesoft.svn.core.auth.ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(subversionUsername, repositoryPassword);
        ourClientManager = SVNClientManager.newInstance(SVN_options, authManager);
        ourClientManager.getCommitClient().setEventHandler(myCommitEventHandler);
        ourClientManager.getUpdateClient().setEventHandler(myUpdateEventHandler);
        ourClientManager.getWCClient().setEventHandler(myWC_EventHandler);
        return true;
    }
    
//=====================================================================
    
	private static long checkout(SVNURL url, SVNRevision revision, File destPath)
    {
        SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
        updateClient.setIgnoreExternals(false);
        long result = -1;
        try
        {
            updateClient.doCheckout(url, destPath, revision, revision, SVNDepth.fromRecurse(true),false);
        }
        catch(Exception e)
        {
            VisualLogger.addErrorToLog(e.toString());
            ++error;
            return -1;
        }
        return result;
    }

//=====================================================================
    
    private static boolean showStatus(File wcPath, boolean isRecursive, boolean isRemote, boolean isReportAll, boolean isIncludeIgnored, boolean isCollectParentExternals)
    {
        try
        {
            myStatusEventHandler = new SVN_StatusHandler(isRemote);
            ourClientManager.getStatusClient().doStatus(wcPath, isRecursive, isRemote, isReportAll, isIncludeIgnored, isCollectParentExternals, myStatusEventHandler);
        }
        catch(Exception e)
        {
            VisualLogger.addErrorToLog(e.toString());
            VisualLogger.addErrorToLogAndWindow("Error getting the repository status");
            ++error;
            return false;
        }
        return true;
    }

//=====================================================================
    
    private boolean checkConnectionWithSubversionRespository()
    {
        try
        {
            repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(repositoryURL));
            org.tmatesoft.svn.core.auth.ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(subversionUsername, repositoryPassword);
            repository.setAuthenticationManager(authManager);
            repository.testConnection();
        }
        catch(Exception e)
        {
            VisualLogger.addErrorToLog(e.toString());
            VisualLogger.addErrorToLogAndWindow("It is not possible to connect with the repository");
            ++error;
            return false;
        }
        VisualLogger.add("The connection with the repository is OK", "[SVN]");
        return true;
    }
    
//=====================================================================
    
    private boolean updateFromSubversionRepository()
    {
        SVNURL url = null;
        try
        {
            VisualLogger.add("Getting the last changes from the repository", "[SVN]");
            url = SVNURL.parseURIDecoded(repositoryURL);
            File file = new File((new StringBuilder()).append(currentPath).append(repositoryLocalPathToUpdate).toString());
            changedFileList.clear();
            checkout(url, SVNRevision.HEAD, file);
            showStatus(file, true, true, false, true, false);
        }
        catch(Exception e)
        {
            VisualLogger.addErrorToLogAndWindow(e.toString());
            ++error;
            return false;
        }
        int maxChangedFileList = changedFileList.size();
        if(changedFileList.size() > 0)
            VisualLogger.addMessageToLogAndWindow((new StringBuilder()).append(maxChangedFileList).append(" file has been updated. See log file in:").append("/Output/Logger").toString(), "[SVN]");
        else{
        	if (error>0) VisualLogger.addMessageToLogAndWindow("An error was found updating from server", "[SVN]");
        	else
        		VisualLogger.addMessageToLogAndWindow("There is nothing to update");
        }
        return true;
    }
    
  //=====================================================================    
}

//=====================================================================
//=====================================================================
//End of file 'myRepository.java'
//=====================================================================