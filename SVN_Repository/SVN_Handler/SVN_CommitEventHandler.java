// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 29/03/2011 11:28:58
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SVN_CommitEventHandler.java

package SVN_Repository.SVN_Handler;

import EGSE_Common.Logger.VisualLogger;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.SVNProperty;
import org.tmatesoft.svn.core.wc.*;

public class SVN_CommitEventHandler
    implements ISVNEventHandler
{

    public void handleEvent(SVNEvent event, double progress)
    {
        SVNEventAction action = event.getAction();
        if(action == SVNEventAction.COMMIT_MODIFIED)
            VisualLogger.add((new StringBuilder()).append("Sending   ").append(event.getURL()).toString(), "SVN");
        else
        if(action == SVNEventAction.COMMIT_DELETED)
            VisualLogger.add((new StringBuilder()).append("Deleting   ").append(event.getURL()).toString(), "SVN");
        else
        if(action == SVNEventAction.COMMIT_REPLACED)
            VisualLogger.add((new StringBuilder()).append("Replacing   ").append(event.getURL()).toString(), "SVN");
        else
        if(action == SVNEventAction.COMMIT_DELTA_SENT)
            VisualLogger.add("Transmitting file data....", "SVN");
        else
        if(action == SVNEventAction.COMMIT_ADDED)
        {
            String mimeType = event.getMimeType();
            if(SVNProperty.isBinaryMimeType(mimeType))
                VisualLogger.add((new StringBuilder()).append("Adding  (bin)  ").append(event.getURL()).toString(), "SVN");
            else
                VisualLogger.add((new StringBuilder()).append("Adding         ").append(event.getURL()).toString(), "SVN");
        }
    }

    public void checkCancelled()
        throws SVNCancelException
    {
    }

    public SVN_CommitEventHandler()
    {
    }
}