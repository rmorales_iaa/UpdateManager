// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 29/03/2011 11:28:58
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SVN_StatusHandler.java

package SVN_Repository.SVN_Handler;

import EGSE_Common.Logger.VisualLogger;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.SVNLock;
import org.tmatesoft.svn.core.wc.*;

public class SVN_StatusHandler
    implements ISVNStatusHandler, ISVNEventHandler
{

    public SVN_StatusHandler(boolean isRemote)
    {
        myIsRemote = isRemote;
    }

    public void handleStatus(SVNStatus status)
    {
        SVNStatusType contentsStatus = status.getContentsStatus();
        String pathChangeType = " ";
        boolean isAddedWithHistory = status.isCopied();
        if(contentsStatus == SVNStatusType.STATUS_MODIFIED)
            pathChangeType = "M";
        else
        if(contentsStatus == SVNStatusType.STATUS_CONFLICTED)
            pathChangeType = "C";
        else
        if(contentsStatus == SVNStatusType.STATUS_DELETED)
            pathChangeType = "D";
        else
        if(contentsStatus == SVNStatusType.STATUS_ADDED)
            pathChangeType = "A";
        else
        if(contentsStatus == SVNStatusType.STATUS_UNVERSIONED)
            pathChangeType = "?";
        else
        if(contentsStatus == SVNStatusType.STATUS_EXTERNAL)
            pathChangeType = "X";
        else
        if(contentsStatus == SVNStatusType.STATUS_IGNORED)
            pathChangeType = "I";
        else
        if(contentsStatus == SVNStatusType.STATUS_MISSING || contentsStatus == SVNStatusType.STATUS_INCOMPLETE)
            pathChangeType = "!";
        else
        if(contentsStatus == SVNStatusType.STATUS_OBSTRUCTED)
            pathChangeType = "~";
        else
        if(contentsStatus == SVNStatusType.STATUS_REPLACED)
            pathChangeType = "R";
        else
        if(contentsStatus == SVNStatusType.STATUS_NONE || contentsStatus == SVNStatusType.STATUS_NORMAL)
            pathChangeType = " ";
        String remoteChangeType = " ";
        if(status.getRemotePropertiesStatus() != SVNStatusType.STATUS_NONE || status.getRemoteContentsStatus() != SVNStatusType.STATUS_NONE)
            remoteChangeType = "*";
        SVNStatusType propertiesStatus = status.getPropertiesStatus();
        String propertiesChangeType = " ";
        if(propertiesStatus == SVNStatusType.STATUS_MODIFIED)
            propertiesChangeType = "M";
        else
        if(propertiesStatus == SVNStatusType.STATUS_CONFLICTED)
            propertiesChangeType = "C";
        boolean isLocked = status.isLocked();
        boolean isSwitched = status.isSwitched();
        SVNLock localLock = status.getLocalLock();
        SVNLock remoteLock = status.getRemoteLock();
        String lockLabel = " ";
        if(localLock != null)
        {
            lockLabel = "K";
            if(remoteLock != null)
            {
                if(!remoteLock.getID().equals(localLock.getID()))
                    lockLabel = "T";
            } else
            if(myIsRemote)
                lockLabel = "B";
        } else
        if(remoteLock != null)
            lockLabel = "O";
        long workingRevision = status.getRevision().getNumber();
        long lastChangedRevision = status.getCommittedRevision().getNumber();
        String offset = "                                ";
        String offsets[] = new String[3];
        offsets[0] = offset.substring(0, 6 - String.valueOf(workingRevision).length());
        offsets[1] = offset.substring(0, 6 - String.valueOf(lastChangedRevision).length());
        offsets[2] = offset.substring(0, offset.length() - (status.getAuthor() == null ? 1 : status.getAuthor().length()));
        VisualLogger.add((new StringBuilder()).append(pathChangeType).append(propertiesChangeType).append(isLocked ? "L" : " ").append(isAddedWithHistory ? "+" : " ").append(isSwitched ? "S" : " ").append(lockLabel).append("  ").append(remoteChangeType).append("  ").append(workingRevision).append(offsets[0]).append(lastChangedRevision < 0L ? "?" : String.valueOf(lastChangedRevision)).append(offsets[1]).append(status.getAuthor() == null ? "?" : status.getAuthor()).append(offsets[2]).append(status.getFile().getPath()).toString(), "SVN");
    }

    public void handleEvent(SVNEvent event, double progress)
    {
        SVNEventAction action = event.getAction();
        if(action == SVNEventAction.STATUS_COMPLETED)
            VisualLogger.add((new StringBuilder()).append("Status against revision:  ").append(event.getRevision()).toString(), "SVN");
    }

    public void checkCancelled()
        throws SVNCancelException
    {
    }

    private boolean myIsRemote;
}