// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 29/03/2011 11:28:58
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SVN_UpdateEventHandler.java

package SVN_Repository.SVN_Handler;

import EGSE_Common.Logger.VisualLogger;
import MyRepository.myRepository;

import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.wc.*;

public class SVN_UpdateEventHandler
    implements ISVNEventHandler
{

    public void handleEvent(SVNEvent event, double progress)
    {
        SVNEventAction action = event.getAction();
        String pathChangeType = " ";
        if(action == SVNEventAction.UPDATE_ADD)
        {
            pathChangeType = "A";
            myRepository.changedFileList.add(event.getFile().getAbsolutePath());
        } else
        if(action == SVNEventAction.UPDATE_DELETE)
            pathChangeType = "D";
        else
        if(action == SVNEventAction.UPDATE_UPDATE)
        {
            SVNStatusType contentsStatus = event.getContentsStatus();
            if(contentsStatus == SVNStatusType.CHANGED)
            {
                pathChangeType = "U";
                myRepository.changedFileList.add(event.getFile().getAbsolutePath());
            } else
            if(contentsStatus == SVNStatusType.CONFLICTED)
                pathChangeType = "C";
            else
            if(contentsStatus == SVNStatusType.MERGED)
                pathChangeType = "G";
        } else
        {
            if(action == SVNEventAction.UPDATE_EXTERNAL)
            {
                VisualLogger.add((new StringBuilder()).append("Fetching external item into '").append(event.getFile().getAbsolutePath()).append("'").toString(), "[SVN]");
                VisualLogger.add((new StringBuilder()).append("External at revision ").append(event.getRevision()).toString(), "[SVN]");
                return;
            }
            if(action == SVNEventAction.UPDATE_COMPLETED)
            {
                VisualLogger.add((new StringBuilder()).append("At revision ").append(event.getRevision()).toString(), "[SVN]");
                return;
            }
            if(action == SVNEventAction.ADD)
            {
                VisualLogger.add((new StringBuilder()).append("A     ").append(event.getURL()).toString(), "[SVN]");
                return;
            }
            if(action == SVNEventAction.DELETE)
            {
                VisualLogger.add((new StringBuilder()).append("D     ").append(event.getURL()).toString(), "[SVN]");
                return;
            }
            if(action == SVNEventAction.LOCKED)
            {
                VisualLogger.add((new StringBuilder()).append("L     ").append(event.getURL()).toString(), "[SVN]");
                return;
            }
            if(action == SVNEventAction.LOCK_FAILED)
            {
                VisualLogger.add((new StringBuilder()).append("failed to lock    ").append(event.getURL()).toString(), "[SVN]");
                return;
            }
        }
        SVNStatusType propertiesStatus = event.getPropertiesStatus();
        String propertiesChangeType = " ";
        if(propertiesStatus == SVNStatusType.CHANGED)
        {
            propertiesChangeType = "U";
            myRepository.changedFileList.add(event.getFile().getAbsolutePath());
        } else
        if(propertiesStatus == SVNStatusType.CONFLICTED)
            propertiesChangeType = "C";
        else
        if(propertiesStatus == SVNStatusType.MERGED)
            propertiesChangeType = "G";
        String lockLabel = " ";
        SVNStatusType lockType = event.getLockStatus();
        if(lockType == SVNStatusType.LOCK_UNLOCKED)
            lockLabel = "B";
        VisualLogger.add((new StringBuilder()).append(pathChangeType).append(propertiesChangeType).append(lockLabel).append("       ").append(event.getURL()).toString(), "[SVN]");
    }

    public void checkCancelled()
        throws SVNCancelException
    {
    }

    public SVN_UpdateEventHandler()
    {
    }
}