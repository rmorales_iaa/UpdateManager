// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 29/03/2011 11:28:58
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SVN_WC_EventHandler.java

package SVN_Repository.SVN_Handler;

import EGSE_Common.Logger.VisualLogger;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.wc.*;

public class SVN_WC_EventHandler
    implements ISVNEventHandler
{

    public void handleEvent(SVNEvent event, double progress)
    {
        SVNEventAction action = event.getAction();
        if(action == SVNEventAction.ADD)
        {
            VisualLogger.add((new StringBuilder()).append("A     ").append(event.getURL()).toString(), "SVN");
            return;
        }
        if(action == SVNEventAction.COPY)
        {
            VisualLogger.add((new StringBuilder()).append("A  +  ").append(event.getURL()).toString(), "SVN");
            return;
        }
        if(action == SVNEventAction.DELETE)
        {
            VisualLogger.add((new StringBuilder()).append("D     ").append(event.getURL()).toString(), "SVN");
            return;
        }
        if(action == SVNEventAction.LOCKED)
        {
            VisualLogger.add((new StringBuilder()).append("L     ").append(event.getURL()).toString(), "SVN");
            return;
        }
        if(action == SVNEventAction.LOCK_FAILED)
        {
            VisualLogger.add((new StringBuilder()).append("failed to lock    ").append(event.getURL()).toString(), "SVN");
            return;
        } else
        {
            return;
        }
    }

    public void checkCancelled()
        throws SVNCancelException
    {
    }

    public SVN_WC_EventHandler()
    {
    }
}