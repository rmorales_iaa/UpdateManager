// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 29/03/2011 11:28:58
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FrameMain.java

package UpdateManager;

//=====================================================================

import EGSE_Common.Logger.VisualLogger;
import MyRepository.myRepository;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

//=====================================================================

public class FrameMain extends JFrame
{

	private JScrollPane jScrollPaneLogger;
    private FlowLayout layout;
    public JFrame frameMain;
    
    //=====================================================================
    
    public FrameMain()
    {
        jScrollPaneLogger = new JScrollPane();
        try
        {
            jbInit();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        new myRepository(jScrollPaneLogger, this);              
    }
    
    //=====================================================================
    
    private void jbInit()
        throws Exception
    {
        setSize(new Dimension(990, 500));
        this.setMinimumSize(new Dimension(990, 500));
        jScrollPaneLogger.setPreferredSize(new Dimension(990, 500));
        getContentPane().add(jScrollPaneLogger);
        setDefaultCloseOperation(0);
    }

    //=====================================================================
}